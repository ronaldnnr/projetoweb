FROM romeoz/docker-apache-php:5.6

RUN apt-get update \
    && apt-get install -y php-mongo \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /var/www/app/
COPY index.php /var/www/app
COPY respostas.php /var/www/app
COPY download.php /var/www/app
ADD ./image_prova /var/www/app
EXPOSE 80 443

CMD ["/sbin/entrypoint.sh"]